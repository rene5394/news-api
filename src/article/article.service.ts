import { Cron } from '@nestjs/schedule';
import axios from 'axios';
import { Model } from 'mongoose';
import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Article, ArticleDocument } from './schemas/article.schema';
import { ArticleSearchParams } from './types/articleSearchParams';
import { paginationLimit } from '../common/constants';

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel(Article.name) private articleModel: Model<ArticleDocument>
  ) {}

  private readonly logger = new Logger(ArticleService.name);

  @Cron('0 0 * * * *')
  async cronCreateBulk() {
    this.logger.debug('Cron job working each hour');

    try {
      const response = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs');
      this.createBulk(response.data.hits);
    } catch (error) {
      console.error(error);
    }
  }

  async createBulk(articles: Article[]) {
    articles.map( async(article) => {
      const oldArticle = await this.findOneByOriginalId(article.objectID);
      if (!oldArticle) {
        article.deleted = false;
        await this.articleModel.create(article);
      }
    });

    return;
  }

  async findAll(query: any): Promise<Article[]> {
    let articleSearchParams : ArticleSearchParams = {};

    if (query.author) articleSearchParams.author = query.author;
    if (query.tag) articleSearchParams._tags = query.tag;
    if (query.title) articleSearchParams.title = query.title;
    
    (query.deleted) ?
    articleSearchParams.deleted = query.deleted :
    articleSearchParams.deleted = false;

    return await this.articleModel.find(articleSearchParams)
      .sort( { created_at: -1 } )
      .skip((query.page -1) * paginationLimit)
      .limit(paginationLimit)
      .exec();
  }

  async findOne(id: string): Promise<Article> {
    try {
      return await this.articleModel.findById(id).exec();
    } catch (error) {
      throw new HttpException('NOT_FOUND', HttpStatus.NOT_FOUND);
    }
  }

  async findOneByOriginalId(id: string): Promise<Article> {
    return await this.articleModel.findOne({objectID: id}).exec();
  }

  async remove(id: string): Promise<Article> {
    try {
      return await this.articleModel.findByIdAndUpdate( id,
        { deleted: true },
        { new: true }
      ).exec();
    } catch (error) {
      throw new HttpException('NOT_FOUND', HttpStatus.NOT_FOUND);
    }
  }
}
