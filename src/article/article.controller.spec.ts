import { Test, TestingModule } from '@nestjs/testing';
import { ArticleController } from './article.controller';
import { ArticleService } from './article.service';
import { HighlightResult } from './schemas/highlightResult.schema';

describe('ArticleController', () => {
  let controller: ArticleController;

  const mockArticleService = {
    findOne: jest.fn(id => {
      return {
      id: 1,
      created_at: new Date(),
      title: 'Node post',
      url: 'node.com',
      author: 'Rene',
      points: '',
      story_text: 'Blog about node',
      comment_text: 'great!',
      num_comments: 2,
      story_id: 200,
      story_title: 'Blog about node',
      story_url: 'node.com/node',
      parent_id: 1,
      created_at_i: 20000,
      _tags: 'comment',
      objectID: '12345',
      deleted: false
      }
    })
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticleController],
      providers: [ArticleService],
    }).overrideProvider(ArticleService).useValue(mockArticleService).compile();

    controller = module.get<ArticleController>(ArticleController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should find an article', () => {
    expect(controller.findOne('1')).toEqual({
      id: 1,
      created_at: expect.any(Date),
      title: expect.any(String),
      url: expect.any(String),
      author: expect.any(String),
      points: expect.any(String),
      story_text: expect.any(String),
      comment_text: expect.any(String),
      num_comments: expect.any(Number),
      story_id: expect.any(Number),
      story_title: expect.any(String),
      story_url: expect.any(String),
      parent_id: expect.any(Number),
      created_at_i: expect.any(Number),
      _tags: expect.any(String),
      objectID: expect.any(String),
      deleted: expect.any(Boolean)
    });
  });
});
