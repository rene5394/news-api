export type ArticleSearchParams = {
  author?: string;
  _tags?: string;
  title?: string;
  deleted?: boolean;
};
