import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export type StoryUrlDocument = StoryUrl & Document;

@Schema()
export class StoryUrl {
  @Prop({ select: false})
  _id: string;
  
  @ApiProperty()
  @Prop()
  value: string;

  @ApiProperty()
  @Prop()
  matchLevel: string;

  @ApiProperty()
  @Prop([String])
  matchedWords: string;
}

export const StoryUrlSchema = SchemaFactory.createForClass(StoryUrl);