import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export type AuthorDocument = Author & Document;

@Schema()
export class Author {
  @Prop({ select: false})
  _id: string;

  @ApiProperty()
  @Prop()
  value: string;

  @ApiProperty()
  @Prop()
  matchLevel: string;

  @ApiProperty()
  @Prop([String])
  matchedWords: string;
}

export const AuthorSchema = SchemaFactory.createForClass(Author);