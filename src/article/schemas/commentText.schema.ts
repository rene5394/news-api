import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export type CommentTextDocument = CommentText & Document;

@Schema()
export class CommentText {
  @Prop({ select: false})
  _id: string;

  @ApiProperty()
  @Prop()
  value: string;

  @ApiProperty()
  @Prop()
  matchLevel: string;

  @ApiProperty()
  @Prop()
  fullyHighlighted: boolean;

  @ApiProperty()
  @Prop([String])
  matchedWords: string;
}

export const CommentTextSchema = SchemaFactory.createForClass(CommentText);