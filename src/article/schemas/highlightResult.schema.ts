import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { Author, AuthorSchema } from './author.schema';
import { CommentText, CommentTextSchema } from './commentText.schema';
import { StoryTitle, StoryTitleSchema } from './storyTitle.schema';
import { StoryUrl, StoryUrlSchema } from './storyUrl';

export type HighlightResultDocument = HighlightResult & Document;

@Schema()
export class HighlightResult {
  @Prop({ select: false})
  _id: string;

  @ApiProperty()
  @Prop({ type: AuthorSchema })
  author: Author;

  @ApiProperty()
  @Prop({ type: CommentTextSchema})
  comment_text: CommentText;

  @ApiProperty()
  @Prop({ type: StoryTitleSchema })
  story_title: StoryTitle;

  @ApiProperty()
  @Prop({ type: StoryUrlSchema })
  story_url: StoryUrl;
}

export const HighlightResultSchema = SchemaFactory.createForClass(HighlightResult);