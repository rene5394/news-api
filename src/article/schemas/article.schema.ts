import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { HighlightResult, HighlightResultSchema } from './highlightResult.schema';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @ApiProperty()
  @Prop()
  created_at: Date;

  @ApiProperty()
  @Prop()
  title: string;

  @ApiProperty()
  @Prop()
  url: string;

  @ApiProperty()
  @Prop()
  author: string;

  @ApiProperty()
  @Prop()
  points: string;

  @ApiProperty()
  @Prop()
  story_text: string;

  @ApiProperty()
  @Prop()
  comment_text: string;

  @ApiProperty()
  @Prop()
  num_comments: number;

  @ApiProperty()
  @Prop()
  story_id: number;

  @ApiProperty()
  @Prop()
  story_title: string;

  @ApiProperty()
  @Prop()
  story_url: string;

  @ApiProperty()
  @Prop()
  parent_id: number;

  @ApiProperty()
  @Prop()
  created_at_i: number;

  @ApiProperty()
  @Prop([String])
  _tags: string[];

  @ApiProperty()
  @Prop()
  objectID: string;

  @ApiProperty()
  @Prop({ type: HighlightResultSchema })
  _highlightResult: HighlightResult;

  @ApiProperty()
  @Prop()
  deleted: boolean;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);