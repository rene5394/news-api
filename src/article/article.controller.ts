import { Controller, Get, Param, Delete, Query } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Article } from './schemas/article.schema';

@ApiTags('articles')
@Controller('v1/articles')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Get()
  @ApiResponse({
    status: 200,
    description: 'Articles has been successfully fetched',
    type: Article
  })
  findAll(@Query() query: any): Promise<Article[]> {
    if (!query.page) {
      query = { page: '1', ...query };
    } if (+query.page < 1) {
      query.page = 1;
    }
    
    return this.articleService.findAll(query);
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    required: true,
    description: 'Should be an id of an article that exists in the database',
    type: String
  })
  @ApiResponse({
    status: 200,
    description: 'An article has been successfully fetched',
    type: Article
  })
  @ApiResponse({
    status: 404,
    description: 'An article with given id does not exist.'
  })
  findOne(@Param('id') id: string): Promise<Article> {
    return this.articleService.findOne(id);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<Article> {
    return this.articleService.remove(id);
  }
}
