# Node Articles API

## Stack

- Framework: NestJS
- Language: TypeScript
- Database: MongoDB
- ORM: TypeORM

## Installation

API configuration

```bash
  cp .env.example .env
  npm install
  npm run build
  npm run start
```

Database configuration

```bash
  Create database called news
  Create the collection articles
```

## Swagger Docs

```bash
  http://localhost:8080/api
```
